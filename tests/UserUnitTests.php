<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserUnitTest extends TestCase
{
    const EMAIL = 'true@gmail.com';
    const PWD = 'password';

    public function testUserTrue()
    {
        $user = new User();
        $date = new \DateTime('2022-03-10T13:09:28.068Z');

        $user->setEmail(self::EMAIL)
            ->setPassword(self::PWD)
            ->setBirthDate($date)
            ->setIdCompany(1)
            ->setIdAddress(2)
            ->setRemainingDays(10);

        $this->assertEquals($user->getEmail(),self::EMAIL);
        $this->assertEquals($user->getPassword() , self::PWD);
        $this->assertEquals($user->getBirthDate() , $date);
        $this->assertEquals($user->getIdCompany() , 1);
        $this->assertEquals($user->getIdAddress() , 2);
        $this->assertEquals($user->getRemainingDays() , 10);
    }
    public function testUserFalse()
    {
        $user = new User();
        $date = new \DateTime('2022-03-10T13:09:28.068Z');

        $user->setEmail(self::EMAIL)
            ->setPassword(self::PWD)
            ->setBirthDate($date)
            ->setIdCompany(1)
            ->setIdAddress(2)
            ->setRemainingDays(10);

        $this->assertNotEquals($user->getEmail() , 'false@gmail.com');
        $this->assertNotEquals($user->getPassword() , 'false');
        $this->assertNotEquals($user->getBirthDate() , '2022-03-14T13:09:28.068Z');
        $this->assertNotEquals($user->getIdCompany() , 4);
        $this->assertNotEquals($user->getIdAddress() , 3);
        $this->assertNotEquals($user->getRemainingDays() , 20);
    }
}
