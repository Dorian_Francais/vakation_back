<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Company;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CompanyUnitTest extends WebTestCase
{
    public function testCompanyIsSuccess()
    {
        $company = new Company();

        $company->setName('CompanyName')
            ->setLegalStatus('SARL')
            ->setIdAddress(2)
            ->setIdManager(1);

        $this->assertEquals("CompanyName", $company->getName());
        $this->assertEquals("SARL", $company->getLegalStatus());
        $this->assertEquals(2, $company->getIdAddress());
        $this->assertEquals(1, $company->getIdManager());
    }
    public function testCompanyIsFailure()
    {
        $company = new Company();

        $company->setName('CompanyName')
            ->setLegalStatus('SARL')
            ->setIdAddress(2)
            ->setIdManager(1);

        $this->assertNotEquals("CompanyNameFail", $company->getName());
        $this->assertNotEquals("SARLFail", $company->getLegalStatus());
        $this->assertNotEquals(3, $company->getIdAddress());
        $this->assertNotEquals(2, $company->getIdManager());
    }
}
