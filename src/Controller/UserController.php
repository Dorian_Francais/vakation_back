<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;


class UserController extends AbstractController
{
    public function __construct(
        private UserService $userService
    )
    {
    }

    #[Route('/api/users/listByTeam/{idTeam}', name: 'listUsersByTeam', methods:"GET")]
    public function listUsersByTeam(string $idTeam): Response
    {
       try{
           $list = $this->userService->listUsersByTeam($idTeam);

           return $this->json(["liste" => $list], Response::HTTP_OK,);
       }catch(Exception $e) {
            return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST,);
        }
    }
}