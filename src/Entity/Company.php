<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
#[ApiResource(
    collectionOperations: ['get','post'],
    itemOperations: ['get','patch','delete']
)]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $legalStatus;

    #[ORM\Column(type: 'integer')]
    private $idAddress;

    #[ORM\Column(type: 'integer')]
    private $idManager;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLegalStatus(): ?string
    {
        return $this->legalStatus;
    }

    public function setLegalStatus(string $legalStatus): self
    {
        $this->legalStatus = $legalStatus;

        return $this;
    }

    public function getIdAddress(): ?int
    {
        return $this->idAddress;
    }

    public function setIdAddress(int $idAddress): self
    {
        $this->idAddress = $idAddress;

        return $this;
    }

    public function getIdManager(): ?int
    {
        return $this->idManager;
    }

    public function setIdManager(int $idManager): self
    {
        $this->idManager = $idManager;

        return $this;
    }
}
