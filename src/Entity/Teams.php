<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TeamsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeamsRepository::class)]
#[ApiResource(
    collectionOperations: ['get','post'],
    itemOperations: ['get','patch','delete']
)]
class Teams
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'bigint')]
    private $capacity;

    #[ORM\Column(type: 'string', length: 255)]
    private $idManager;

    #[ORM\Column(type: 'string', length: 255)]
    private $idCompany;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCapacity(): ?string
    {
        return $this->capacity;
    }

    public function setCapacity(string $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getIdManager(): ?string
    {
        return $this->idManager;
    }

    public function setIdManager(string $idManager): self
    {
        $this->idManager = $idManager;

        return $this;
    }

    public function getIdCompany(): ?string
    {
        return $this->idCompany;
    }

    public function setIdCompany(string $idCompany): self
    {
        $this->idCompany = $idCompany;

        return $this;
    }
}
