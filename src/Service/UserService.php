<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;


class UserService
{
    public function __construct(
        private UserRepository         $userRepository,
        private EntityManagerInterface $manager
    )
    {
    }

    public function listUsersByTeam($idTeam)
    {
        try{
            return $this->userRepository->findUsersByTeam($idTeam);
        }catch(Exception $e) {
             return $this->json(["message" => $e->getMessage()], Response::HTTP_BAD_REQUEST,);
         }
    }
}